def load_mnist(csv_filename):
    """Load the MNIST dataset."""
    raise NotImplementedError


def split_data(x, y, fract_tr=0.5):
    """Split the data X,y into two random subsets."""
    raise NotImplementedError


class NMC(object):

    def __init__(self):
        self._centroids = None

    @property
    def centroids(self):
        return self._centroids

    @centroids.setter
    def centroids(self, value):
        self._centroids = value

    def fit(self, x, y):
        """Fitting the NMC classifier to data by
        estimating centroids for each class."""
        raise NotImplementedError

    def predict(self, x):
        """Predict the class of each input."""
        raise NotImplementedError
